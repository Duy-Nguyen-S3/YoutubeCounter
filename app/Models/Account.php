<?php

namespace App\Models;

use DB;
class Account
{
    public static function checkLogin($username, $pass) {
        $query = array(
            'taikhoan' => $username,
            'matkhau' => $pass
        );
        return DB::table('account')->where($query)->first();
    }

    public static function getWorkStation() {
        $query = array(
            'phanquyen' => 1
        );
        return DB::table('account')->where($query)->get();
    }

    public static function getDetail($id) {
        $query = array(
            'id' => $id
        );
        return DB::table('account')->where($query)->first();
    }
}
