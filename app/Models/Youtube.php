<?php

namespace App\Models;

use Session;
use DB;

class Youtube
{
    public static function getList() {
        $res = DB::table('youtube_link')->get();
        return $res;
    }

    public static function addLink($name, $link) {
        return DB::table('youtube_link')->insert( 
            ['tenvideo' => $name,
             'url' => $link,
             'ngaytao' => time()] );
    }
}
