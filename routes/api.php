<?php
use Illuminate\Http\Request;

Route::get('get/url', 'Api\ApiController@getListURL');
Route::get('client/{id}/{in}/{out}/{key}', 'Api\ApiController@checkoutClient');