﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoadLinkYoutube
{


    public class Rootobject
    {
        public List<Youtube> Result { get; set; }
    }

    public class Youtube : IEquatable<Youtube>
    {
        public int id { get; set; }
        public string tenvideo { get; set; }
        public string url { get; set; }
        public int ngaytao { get; set; }
        public int configtime { get; set; }

        public Youtube(int _id,string _url)
        {
            id = _id;
            url = _url;
        }

        public bool Equals(Youtube other)
        {
            // Would still want to check for null etc. first.
            return this.url == other.url;
        }
    }

}
