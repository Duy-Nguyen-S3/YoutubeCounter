﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using CefSharp;
using CefSharp.WinForms;
using System.Net.Http;
using Newtonsoft.Json;
using System.Net.Http.Headers;

namespace LoadLinkYoutube
{
    public partial class Form1 : Form
    {
        #region Initial
        public ChromiumWebBrowser browser;
        Timer t = new Timer();
        Timer t1 = new Timer();
        static HttpClient client = new HttpClient();
        string startTimeSec = null;

        public List<Youtube> newList = new List<Youtube>();
        public List<Youtube> oldList = new List<Youtube>();
        public List<Youtube> currentList = new List<Youtube>();

        #endregion

        public Form1()
        {
            InitializeComponent();

            lblStartTime.Text = FormatTime(DateTime.Now.Hour,
                DateTime.Now.Minute, DateTime.Now.Second);
            startTimeSec = ConvertToUnixTimestamp(DateTime.Now);

            t.Interval = 1000;
            t1.Interval = 5000;

            t.Tick += new EventHandler(this.t_Tick);
            t1.Tick += new EventHandler(this.t1_Tick);
            t.Start();

            CefSettings settings = new CefSettings();
            Cef.Initialize(settings);
            browser = new ChromiumWebBrowser(txtUrl.Text);
            this.panelEx1.Controls.Add(browser);
            browser.Dock = DockStyle.Fill;
            browser.AddressChanged += Browser_AddressChanged;
        }

        private void Browser_AddressChanged(object sender, AddressChangedEventArgs e)
        {
            this.Invoke(new MethodInvoker(() =>
            {
                txtUrl.Text = e.Address;

            }));

        }

        private void t_Tick(object sender, EventArgs e)
        {
            List<string> lstOldUrl = new List<string>();
            int hh = DateTime.Now.Hour;
            int mm = DateTime.Now.Minute;
            int ss = DateTime.Now.Second;

            int hh1 = 0;
            int mm1 = 0;
            int ss1 = 0;

            string timer = "";

            lblCurrentTime.Text = FormatTime(hh, mm, ss);

            List<Youtube> result = GetApiJson();


            dataGridViewX1.DataSource = result;

            List<Youtube> oldList = dataGridViewX1.DataSource as List<Youtube>;
            List<Youtube> newList = new List<Youtube>()
            {
                new Youtube(2,"https://www.youtube.com/watch?v=pnVbYyRG1R0&list=RDMMpnVbYyRG1R0"),
                new Youtube(3,"c"),
            };

            foreach (Youtube item in newList)
            {
                if (!oldList.Contains(item))
                {
                    result.Add(item);
                }
            }
            BindList(result);
        }

        private void t1_Tick(object sender, EventArgs e)
        {

            int hh = DateTime.Now.Hour;
            int mm = DateTime.Now.Minute;
            int ss = DateTime.Now.Second;


            lblCurrentTime.Text = FormatTime(hh, mm, ss);

            newList = GetApiJson();

            List<Youtube> oldList = dataGridViewX1.DataSource as List<Youtube>;
            //List<Youtube> newList = new List<Youtube>()
            //{
            //    new Youtube(2,"https://www.youtube.com/watch?v=pnVbYyRG1R0&list=RDMMpnVbYyRG1R0"),
            //    new Youtube(3,"c"),
            //};

            foreach (Youtube item in newList)
            {
                if (!oldList.Contains(item))
                {
                    oldList.Add(item);
                }
            }

            BindList(oldList);
        }

        public void BindList(List<Youtube> result)
        {
            //var bindingList = new BindingList<Youtube>(result);
            var source = new BindingSource();
            source.DataSource = result;
            dataGridViewX1.DataSource = source;
        }
        private void btnGo_Click(object sender, EventArgs e)
        {
            browser.Load(txtUrl.Text.Replace("watch?v=", "embed/"));
        }

        public void buttonX1_Click(object sender, EventArgs e)
        {
            List<Youtube> result = GetApiJson();

            //foreach (var item in result)
            //{
            //    var a = ConvertFromUnixTimestamp((double)item.ngaytao);
            //    MessageBox.Show(ConvertToUnixTimestamp(a).ToString());
            //}

            var bindingList = new BindingList<Youtube>(result);
            var source = new BindingSource(bindingList, null);
            dataGridViewX1.DataSource = source;

        }

        #region Helper


        public static List<Youtube> GetApiJson()
        {
            string apiUrl = "http://123.28.114.134/YoutubeCounter/api/get/url";

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(apiUrl);

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = client.GetAsync(apiUrl).Result;
            if (response.IsSuccessStatusCode)
            {
                var result = response.Content.ReadAsStringAsync();
                var rootResult = JsonConvert.DeserializeObject<List<Youtube>>(result.Result);

                return rootResult;
            }
            else
                return null;

        }

        public static void PostApiJson()
        {
            string apiUrl = "http://123.28.114.134/YoutubeCounter/api/get/url";

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(apiUrl);

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));

            //HttpResponseMessage response = client.PostAsync(apiUrl).Result;
            //if (response.IsSuccessStatusCode)
            //{
            //    var result = response.Content.ReadAsStringAsync();

            //}

        }

        public string FormatTime(int hh, int mm, int ss)
        {
            string crtime = "";

            if (hh < 10)
            {
                crtime += "0" + hh;
            }
            else
            {
                crtime += hh;
            }
            crtime += ":";

            if (mm < 10)
            {
                crtime += "0" + mm;
            }
            else
            {
                crtime += mm;
            }
            crtime += ":";

            if (ss < 10)
            {
                crtime += "0" + ss;
            }
            else
            {
                crtime += ss;
            }

            return crtime;
        }

        public static DateTime ConvertFromUnixTimestamp(double timestamp)
        {
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            return origin.AddSeconds(timestamp);
        }

        public static string ConvertToUnixTimestamp(DateTime date)
        {
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            TimeSpan diff = date.ToUniversalTime() - origin;
            return Math.Floor(diff.TotalSeconds).ToString();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Cef.Shutdown();
            //Statistic postData = new Statistic();
            //postData.user_id = 1;
            //postData.giovao = Int32.Parse(startTimeSec);
            //postData.giora = Int32.Parse(ConvertToUnixTimestamp(DateTime.Now));

            //e.Cancel = MessageBox.Show("Are you sure you want to really exit ? ",
            //    "Exit", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No;
        }

        #endregion


    }
}
