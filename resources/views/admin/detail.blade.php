@extends('admin.layout')
@section('content')
<div id="page-wrapper" style="min-height: 290px;">
   <div class="graphs">
      <div class="xs">
         <h3>Phòng máy {{ $data->id }}</h3>
         <div class="bs-example" data-example-id="form-validation-states">
         </div>
         <div class="panel-body">
            <form role="form" class="form-horizontal">
               <div class="form-group">
                  <label class="col-md-2 control-label">Họ tên</label>
                  <div class="col-md-8">
                     <div class="input-group">
                        <div class="alert alert-info" style="margin-bottom: -20px;" role="alert">
                           {{ $data->hoten }}
                        </div>
                     </div>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-md-2 control-label">CMND</label>
                  <div class="col-md-8">
                     <div class="input-group">
                        <div class="alert alert-info" style="margin-bottom: -20px;" role="alert">
                           {{ $data->cmnd }}
                        </div>
                     </div>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-md-2 control-label">Địa chỉ</label>
                  <div class="col-md-8">
                     <div class="input-group">
                        <div class="alert alert-info" style="margin-bottom: -20px;" role="alert">
                           {{ $data->diachi }}
                        </div>
                     </div>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-md-2 control-label">Số điện thoại</label>
                  <div class="col-md-8">
                     <div class="input-group">
                        <div class="alert alert-info" style="margin-bottom: -20px;" role="alert">
                           {{ $data->sdt }}
                        </div>
                     </div>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-md-2 control-label">Số tài khoản</label>
                  <div class="col-md-8">
                     <div class="input-group">
                        <div class="alert alert-info" style="margin-bottom: -20px;" role="alert">
                           {{ $data->stk }}
                        </div>
                     </div>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-md-2 control-label">Tên ngân hàng</label>
                  <div class="col-md-8">
                     <div class="input-group">
                        <div class="alert alert-info" style="margin-bottom: -20px;" role="alert">
                           {{ $data->nganhang }}
                        </div>
                     </div>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-md-2 control-label">Tài khoản</label>
                  <div class="col-md-8">
                     <div class="input-group">
                        <div class="alert alert-info" style="margin-bottom: -20px;" role="alert">
                           {{ $data->taikhoan }}
                        </div>
                     </div>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-md-2 control-label">Tổng lượt xem</label>
                  <div class="col-md-8">
                     <div class="input-group">
                        <div class="alert alert-info" style="margin-bottom: -20px;" role="alert">
                           Tính năng đã bị tắt
                        </div>
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
@endsection