@extends('admin.layout')

@section('content')
<div id="page-wrapper">
    <div class="graphs">
    <div class="xs">
       <h3>Thêm link Youtube</h3>
       <div class="tab-content">
          <div class="tab-pane active" id="horizontal-form">
            <form class="form-horizontal" action="{{ route('addlink') }}" method="POST">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="focusedinput" class="col-sm-2 control-label">Tên video (nếu có)</label>
                    <div class="col-sm-8">
                       <input type="text" class="form-control1" id="name" name="name" placeholder="Thêm ở đây">
                    </div>
                 </div>
                 <div class="form-group">
                        <label for="focusedinput" class="col-sm-2 control-label">Link</label>
                        <div class="col-sm-8">
                           <input type="text" class="form-control1" id="link" name="link" placeholder="Thêm ở đây">
                        </div>
                     </div>
             <div class="panel-footer">
                <div class="row">
                   <div class="col-sm-8 col-sm-offset-2">
                      <button class="btn-success btn">Thêm</button>
                   </div>
                </div>
             </div>
          </form>
       </div>
    </div>
@endsection
